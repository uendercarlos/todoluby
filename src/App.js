import React, { useState } from "react";
import { Formik, Form, ErrorMessage } from "formik";
import * as Yup from "yup";

import {
  PageWrapper,
  Title,
  Label,
  Input,
  StyledInlineErrorMessage,
  Submit,
  CodeWrapper,
} from "./styles";

function App() {
  const [formValues, setFormValues] = useState();

  return (
    <PageWrapper>
      <Title>
      Projeto estilizado com styled components, contem um
      campo de texto e um botão para adicionar tarefa.
      Está sendo validado o campo com o yup para ter no mínimo 5 caracteres
      e não ficar vazio. Foi usado o formik para criar o formulário
      </Title>
     
     
      <Formik
        initialValues={{
          tarefa: "",
          
        }}
        validationSchema={Yup.object().shape({
          tarefa: Yup.string()
            .min(5, "A tarefa deve conter no minimo 5 caracteres")
            .required("Campo obrigatorio"),
         
        })}
        onSubmit={(values, actions) => {
          console.log(values);
          setFormValues(values);

          const timeOut = setTimeout(() => {
            actions.setSubmitting(false);

            clearTimeout(timeOut);
          }, 1000);
        }}
      >
        {({
          values,
          errors,
          touched,
          handleSubmit,
          isSubmitting,
          isValidating,
          isValid,
        }) => {
          return (
            <>
              <Form name="contact" method="post" onSubmit={handleSubmit}>
                <Label htmlFor="tarefa">
                  
                  <Input
                    type="text"
                    name="tarefa"
                    autoCorrect="off"
                    autoComplete="name"
                    placeholder="Digite sua tarefa aqui"
                    valid={touched.tarefa && !errors.tarefa}
                    error={touched.tarefa && errors.tarefa}
                  />
                </Label>
                {errors.tarefa && touched.tarefa && (
                  <StyledInlineErrorMessage>
                    {errors.tarefa}
                  </StyledInlineErrorMessage>
                )}
             
                <ErrorMessage name="email">
                  {(msg) => (
                    <StyledInlineErrorMessage>{msg}</StyledInlineErrorMessage>
                  )}
                </ErrorMessage>
                <Submit type="submit" disabled={!isValid || isSubmitting}>
                  {isSubmitting ? `Submiting...` : `Adicionar tarefa`}
                </Submit>
              </Form>

              <hr />
              <CodeWrapper>
                <strong>Errors:</strong> {JSON.stringify(errors, null, 2)}
                <strong>Touched:</strong> {JSON.stringify(touched, null, 2)}
                {formValues && <strong>Submitted values:</strong>}
                {JSON.stringify(formValues, null, 2)}
              </CodeWrapper>
            </>
          );
        }}
      </Formik>
    </PageWrapper>
  );
}

export default App;
           